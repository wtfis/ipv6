**Donnerstag, 25.05.2023  SprintPlanning**

- Blocker: Einarbeitung ins Lernfeld

IPAM Erstellung alle auf Stand bringen (Anforderung)  
IPv6 Aufarbeitung  
Algorithmus IPv6-Subnetting Wiki Upload  
Tools festlegen  

**Freitag, 26.05.23 Daily**

- Blocker:  IPAM-Erstellung und Anlesen der nötigen Skills

Lucas:      Einarbeitung ins Lernfeld  
Samuel:     Einarbeitung ins Lernfeld  
Paul:       Einarbeitung ins Lernfeld  
Nils:       Krank  
Claudio:    Einarbeitung ins Lernfeld  

**Dienstag, 30.05.23 Daily**

- Blocker:  IPAM-Erstellung und Anlesen der nötigen Skills

Lucas:      IPAM-Erstellung  
Samuel:     IPAM-Erstellung  
Paul:       IPAM-Erstellung  
Nils:       IPAM-Erstellung  
Claudio:    IPAM-Erstellung  

**Mittwoch, 31.05.23 Daily**

- Blocker:  EUR-Workshop, IPAM finalisieren

Lucas:      EUR-Workshop  
Samuel:     EUR-Workshop  
Paul:       EUR-Workshop  
Nils:       EUR-Workshop  
Claudio:    EUR-Workshop  

**Donnerstag, 01.06.23 Daily**

- Blocker:  IPAM finalisieren, Code für die Routerconfig, PT probieren und Baseline bauen

Lucas:      IPAM finalisieren, Router-Config-Code  
Samuel:     IPAM finalisieren, Router-Config-Code  
Paul:       IPAM finalisieren, Router-Config-Code  
Nils:       IPAM finalisieren, Router-Config-Code  
Claudio:    IPAM finalisieren, Router-Config-Code  

**Freitag, 02.06.23 Daily**

- Blocker: Code für die Routerconfig, PT Vertiefung

Lucas:      Python-Kurs  
Samuel:     Organisation & Struktur  
Paul:       Organisation & Struktur  
Nils:       Python-Kurs, Onboarding nach Krankheit  
Claudio:    Python-Kurs  

**Montag, 05.06.23 Daily**

- Blocker: Organisation für den Abschluss diesen Freitag, Code&Config finalisieren

Lucas:      Organisation & Struktur, Code&Config finalisieren  
Samuel:     Organisation & Struktur, Code&Config finalisieren  
Paul:       Organisation & Struktur, Code&Config finalisieren  
Nils:       Organisation & Struktur, Code&Config finalisieren  
Claudio:    Organisation & Struktur, Code&Config finalisieren  

**Dienstag, 06.06.23 Daily**

- Blocker: Code&Config überprüfen, generelles Testing der aktuellen Config mit SVI & OSPFv3

Lucas:      Blocker bearbeiten  
Samuel:     Blocker bearbeiten  
Paul:       Blocker bearbeiten  
Nils:       Blocker bearbeiten  
Claudio:    Blocker bearbeiten  

**Mittwoch, 07.06.23 Daily**

- Blocker: Code&Config überprüfen, generelles Testing der aktuellen Config mit SVI & OSPFv3, Englisch Abgaben

Lucas:      Blocker bearbeiten, Englisch  
Samuel:     Blocker bearbeiten  
Paul:       Blocker bearbeiten, Englisch  
Nils:       Blocker bearbeiten, Englisch  
Claudio:    Blocker bearbeiten, Englisch  

**Donnerstag, 08.06.23 Daily**

- Blocker: OSPFv3 & Static Router abschließen und testen

Lucas:  Tickets in "testing" testen  
Samuel: ACL finalisieren und testen, Eventuell DNS / DHCP beginnen  
Paul:  OSPFv3 fertig machen und testen, PT-Versionen  
Nils: Netzwerkplan erkundigen und beginnen  
Claudio: ACL anschauen / mitmachen, Repo sortieren, Eventuell DNS / DHCP beginnen  

**Freitag, 09.06.23 Daily**

- Blocker: OSPFv3 & Static Router abschließen und testen

Lucas:      Testing  
Samuel:     ACL finalisieren und testen, DNS / DHCP  
Paul:       OSPFv3 fertig machen und testen, PT-Versionen  
Nils:       Netzwerkplan abschließen  
Claudio:    ACL, DNS / DHCP  

**Montag, 12.06.23 Daily**

- Blocker: DHCPv6 Anforderung, IPAM aktualisieren 

Lucas:      DHCPv6  
Samuel:     DHCPv6  
Paul:       Testing  
Nils:       Überarbeitung Netzplan, DHCPv6  
Claudio:    DHCPv6, IPAM  

**Dienstag, 13.06.23 Daily**

- Blocker: DHCPv6 abschließen und testen, CnC + IPAM hübsch machen, letzte Tests und Vorbereitungen

Lucas:      DHCPv6,Testing  
Samuel:     DHCPv6, Testing  
Paul:       Testing, Formatierung der Dokumente  
Nils:       Überarbeitung Netzplan, Testing  
Claudio:    DHCPv6, Testing  

**Mittwoch, 14.06.23 Daily**

- Blocker:  Präsentationsvorbereitung

Lucas:      Präsentation
Samuel:     Präsentation
Paul:       Präsentation
Nils:       Präsentation
Claudio:    Präsentation